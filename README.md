## Practica de configuración de un proyecto

Practica consistente en:

* Crear un repositorio en bitbucket
* Clonar el repo localmente
* Añadir un README.md descriptivo
* Inicializar npm (package.json) con punto de entrada "index.html"
* Instalar el servidor web de pruebas lite-server y establecerlo como dependencia de desarrollo en "package.json" usando el flag "--save-dev" al instalarlo.
* Configurar el alias "start" (npm run lite | lite: lite-server) para poder iniciarlo más facilmente
* Crear el archivo ".gitignore para que git no gestione la carpeta "node_modules"
* Añadir el archivo "index.html" al proyecto
* Instalar bootstrap, jquery y popper.js, y establecerlos como dependencias en "package.json" (opción --save)
* Modificar "index.html" para que referencie las bibliotecas instaladas mediante npm
* Hacer commit y push en los momentos oportunos
